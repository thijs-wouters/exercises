require File.expand_path('../app/exercises.rb', __FILE__)
use Rack::Static, :urls => %w(/stylesheets /images), :root => 'public'
use Rack::ShowExceptions
run Exercises.new
