require 'sinatra/base'
require 'sinatra/activerecord'
require 'gibbon'

class Subscriber < ActiveRecord::Base
end

class Exercise
  attr_reader :lhs, :rhs, :solution

  def initialize(lhs, rhs)
    @lhs = lhs
    @rhs = rhs
  end

  def solve(solution)
    @solution = solution
  end

  def correct?
    solution == lhs * rhs
  end

  def solved?
    !solution.nil?
  end

  def to_s
    "#{lhs} x #{rhs}"
  end
end

class Exercises < Sinatra::Base
  register Sinatra::ActiveRecordExtension

  set :static, true
  set :root, File.dirname(__FILE__)

  NUMBER_OF_EXERCISES = 30

  get '/' do
    erb :'index'
  end

  post '/subscribers' do
    gb = Gibbon::API.new('f186deb81630b2dc370be31e630ebac3-us9', throws_exceptions: false)
    gb.lists.subscribe(id: '721651e6a2', email: {email: params['email']})
    Subscriber.create(email: params['email'])
    redirect '/subscribers/optin'
  end

  get '/subscribers/optin' do
    erb :'optin'
  end

  get '/exercises' do
    if params['ref'] == 'subscribe'
      flash = 'Bedankt om je in te schrijven! We houden je op de hoogte van verdere ontwikkelingen bij Tomin.be.'
    else
      flash = nil
    end
    tables = params['tables'] || (1..10).to_a
    exercises = []
    NUMBER_OF_EXERCISES.times do
      exercises << Exercise.new(rand(1..10), tables.shuffle.first)
    end
    erb :'exercises/index', locals: {tables: params['tables'] || [], exercises: exercises, flash: flash}
  end

  post '/exercises' do
    exercises = []
    NUMBER_OF_EXERCISES.times do |i|
      new_exercise = Exercise.new(params['exercise-lhs'][i.to_s].to_i, params['exercise-rhs'][i.to_s].to_i)
      solution = params['exercise-solution'][i.to_s]
      new_exercise.solve(solution.to_i) unless solution.length == 0
      exercises << new_exercise
    end
    if exercises.all?(&:solved?)
      number_correct = exercises.count(&:correct?)
      case (number_correct*100/NUMBER_OF_EXERCISES).to_i
        when 100
          flash = "#{number_correct}/#{NUMBER_OF_EXERCISES}! Alles juist, goed gedaan!"
        when 90...100
          flash = "#{number_correct}/#{NUMBER_OF_EXERCISES}! Bijna alles juist. Goed gedaan!"
        when 60...90
          flash = "#{number_correct}/#{NUMBER_OF_EXERCISES}. Blijf oefenen!"
        else
          flash = "#{number_correct}/#{NUMBER_OF_EXERCISES}. Dat is niet zo goed. Blijven oefenen!"
      end
    else
      flash = nil
    end
    erb :'exercises/index', locals: {tables: params['tables'] || [], exercises: exercises, flash: flash}
  end
end
